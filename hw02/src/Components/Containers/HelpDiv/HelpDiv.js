import React, {Component} from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

class HelpDiv extends Component {

    constructor() {
        super();
        this.InnerDiv = styled.div`
            text-align: center;
        `;
    }

    render() {
        return (
            <this.InnerDiv >
                {this.props.children}
            </this.InnerDiv>
        );
    }

}

HelpDiv.propTypes = {
    children: PropTypes.element,
}

export default HelpDiv;