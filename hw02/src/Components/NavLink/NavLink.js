import React, { Component } from 'react';
import styled from 'styled-components';

const Link = styled.a`
    padding: 0 25px;
    font-family: tahoma, sans-serif;
    font-size: 16px;
    font-weight: 700;
    `;

class NavLink extends Component {

    render() {
        return (
            <Link>
                {this.props.title}
            </Link>
        )
    }
}

export default NavLink;