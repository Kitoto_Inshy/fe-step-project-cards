import React, {Component} from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

class Text extends Component {

    constructor() {
        super();
        this.TextInner = styled.p`
            font-size: 14px;
            color: #fff;
            text-align: center;
            line-height: 2;
        `;
    }

    render() {
        return (
            <this.TextInner>
                {this.props.text}
            </this.TextInner>
        )
    }
}

Text.propTypes = {
    text: PropTypes.string,
}

export default Text;