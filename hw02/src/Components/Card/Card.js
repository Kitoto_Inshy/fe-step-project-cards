import React, { Component } from 'react';
import styled from 'styled-components';

const Cardo = styled.div`
    padding: 1vw;
    border: 1px solid #666666;
    border-radius: 10px;
    background-color: #ffe5e5;
    `;

const CardImg = styled.img`
    width: 260px;
    margin: 0 auto 15px auto;
    `;

const Artist = styled.h3`
    margin: 0 0 10px 0;
    font-family: tahoma, sans-serif;
    font-size: 18px;
    font-weight: 700;
    `;

const AlbumTitle = styled.h3`
    margin: 0 0 10px 0;
    font-family: tahoma, sans-serif;
    font-size: 16px;
    font-weight: 700;
    `;

const HelpWrap = styled.div`
        display: flex;
        justify-content: space-between;
    `;

const CatalogNumber = styled.h3`
    margin: 0 0 10px 0;
    font-family: tahoma, sans-serif;
    font-size: 14px;
    font-weight: 400;
    `;

const Price = styled.h3`
    display: inline-block;
    width: 56px;
    margin: 0px;
    font-family: tahoma, sans-serif;
    font-size: 16px;
    `;

const CardButton = styled.button`
    display: inline-block;
    border: 1px solid transparent;
    border-radius: 3px;
    font-weight: 700;
    background-color: #d3eefd;
    `;

class Card extends Component {

    render() {
        return (
            <Cardo>
                <CardImg src={this.props.src} />
                <Artist>
                    {this.props.artist}
                </Artist>
                <AlbumTitle>
                    {this.props.title}
                </AlbumTitle>
                <CatalogNumber>
                    Catalog number: {this.props.number}
                </CatalogNumber>
                <HelpWrap>
                    <Price>
                        {this.props.price}
                    </Price>
                    <CardButton>
                        Add to Cart
                    </CardButton>
                    <i class="far fa-star"></i>
                </HelpWrap>
            </Cardo>
        )
    }
}

export default Card;