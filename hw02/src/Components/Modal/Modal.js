import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import HeaderH2 from '../Header/HeaderH2/HeaderH2';
import Text from '../Text/Text';
import ButtonModal from '../Button/ButtonModal';

class Modal extends Component {

    constructor() {
        super();
        this.Wrap = styled.div`
            position: fixed;
            left: 0;
            top: 0;
            display: flex;
            width: 100%;
            height: 100%;
            justify-content: center;
            align-items: center;
            background-color: rgba(0,0,0,0.6);
            z-index: 5;
        `;
        this.CloseButton = styled.button`
            position: absolute;
            top: 0;
            right: 0;
            padding: 10px 10px;
            border: none;
            font-size: 20px;
            font-weight: 700;
            color: #fff;
            background-color: transparent;
            z-index: 5;
        `;
        this.ModalBody = styled.div`
            position: relative;
            width: 520px;
            margin: 0 auto;
        `;
        this.TextWrap = styled.div`
            padding: 35px 0;
            background-color: #e74c3c;
        `;
        this.ButtonsWrap = styled.div`
            padding-bottom: 25px;
            text-align: center;
            background-color: #e74c3c;
        `;
    }

    render() {
        if (!this.props.show) {
            return null;
        } else {
            return (
                <this.Wrap onClick={this.props.hide}>
                    <this.ModalBody>
                        <this.CloseButton onClick={this.props.hide}>
                            &#10006;
                        </this.CloseButton>
                        <HeaderH2 title={this.props.header} />
                        <this.TextWrap>
                            <Text text={this.props.text1} />
                            <Text text={this.props.text2} />
                        </this.TextWrap>
                        <this.ButtonsWrap>
                            {this.props.buttons.map((elem) => <ButtonModal text={elem.text} backgroundColor={elem.backgroundColor} />)}
                        </this.ButtonsWrap>
                    </this.ModalBody>
                </this.Wrap >
            )
        }
    }

}

Modal.propTypes = {
    show: PropTypes.bool,
    hide: PropTypes.func,
    header: PropTypes.string,
    text1: PropTypes.string,
    text2: PropTypes.string,
    buttons: PropTypes.array,
}

export default Modal;

