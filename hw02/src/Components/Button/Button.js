import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

class Button extends Component {

    constructor(props) {
        super();
        this.ButtonInner = styled.button`
        margin: 10px;
        padding: 5px;
        border: none;
        border-radius: 5px;
        font-size: 18px;
        color: #fff;
        background: ${props.backgroundColor};
        `;
    }

    render() {
        return (
            <this.ButtonInner onClick={this.props.todo}>
                {this.props.text}
            </this.ButtonInner>
        )
    }

}

Button.propTypes = {
    backgroundColor: PropTypes.string,
    todo: PropTypes.func,
    text: PropTypes.string,
}

export default Button;