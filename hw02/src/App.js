import React, { Component } from 'react';
import Container from './Container/Container';
import Header from './Sections/Header/Header';
import NavBar from './Sections/NavBar/NavBar';
import Main from './Sections/Main/Main';
import NavLink from './Components/NavLink/NavLink';
import Card from './Components/Card/Card';
import Img from './_imgs/band-maid.jpg';

class App extends Component {

    render() {
        return (
            <Container>
                <Header />
                <NavBar>
                    <NavLink title='Home'/>
                    <NavLink title='Shop'/>
                    <NavLink title='Artists'/>
                    <NavLink title='Forum'/>
                    <NavLink title='Top 10'/>
                    <NavLink title='IRC'/>
                    <NavLink title='Radio'/>
                    <NavLink title='Help'/>
                </NavBar>
                <Main>
                    <Card src={Img} artist='Band-Maid' title='CONQUEROR' number='025846' price='$15.00'/>
                    <Card src={Img} artist='Band-Maid' title='CONQUEROR' number='025846'/>
                    <Card src={Img} artist='Band-Maid' title='CONQUEROR' number='025846'/>
                    <Card src={Img} artist='Band-Maid' title='CONQUEROR' number='025846'/>
                    <Card src={Img} artist='Band-Maid' title='CONQUEROR' number='025846' price='$15.99'/>
                </Main>
            </Container>
        )
    }
}

export default App;
