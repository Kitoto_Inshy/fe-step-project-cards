import React, { Component } from 'react';
import styled from 'styled-components';

const Container = styled.div`
    display: grid;
    grid-template-columns: 1fr 1fr 1fr 1fr;
    justify-items: center;
    grid-row-gap: 12px;
    width: 1280px;
    margin: 0 auto;
    padding: 20px;
    border: 1px solid #666666;
    border-radius: 0 20px;
    box-sizing: border-box;
`;

class Main extends Component {

    render() {
        return (
            <Container>
                {this.props.children}
            </Container>
        )
    }
}

export default Main;