import React, { Component } from 'react';
import styled from 'styled-components';
import Logo from '../../Imgs/logo.png';

const Head = styled.section`
    width: 715px;
    height: 150px;
    margin: 0 auto;
    background-image: url(${Logo});
`;

class Header extends Component {

    render() {
        return (
            <Head />
        )
    }
}

export default Header;