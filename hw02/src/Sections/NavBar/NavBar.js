import React, { Component } from 'react';
import styled from 'styled-components';

const NavBarMainElem = styled.nav`
    max-width: 1280px;
    border: 1px solid transparent;
    border-radius: 20px 0;
    margin: 0 auto 50px auto;
    background-color: #d3eefd;
    `;

const NavBarElemWrap = styled.div`
    display: flex;
    justify-content: center;
    width: 1280px;
    margin: 0 auto;
    padding: 20px;
    box-sizing: border-box;
    `;

class NavBar extends Component {
    // constructor() {
    //     super();


    // }

    render() {
        return (
            <NavBarMainElem>
                <NavBarElemWrap>
                    {this.props.children}
                </NavBarElemWrap>
            </NavBarMainElem>
        )
    }
}

export default NavBar;