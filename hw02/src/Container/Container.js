import React, { Component } from 'react';
import styled from 'styled-components';

class Container extends Component {
    constructor() {
        super();
        this.MainContainer = styled.div`
        margin: 0 auto;
        `;
    }

    render() {
        return (
            <this.MainContainer>
                {this.props.children}
            </this.MainContainer>
        )
    }
}

export default Container;