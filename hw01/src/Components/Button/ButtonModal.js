import React, {Component} from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

class ButtonModal extends Component {

    constructor(props) {
        super();
        this.ButtonInner = styled.button`
            width: 100px;
            margin: 10px;
            padding: 5px;
            border: none;
            border-radius: 5px;
            font-size: 14px;
            color: #fff;
            text-align: center;
            background: ${props.backgroundColor}
        `;
    }

    render() {
        return (
            <this.ButtonInner>
                {this.props.text}
            </this.ButtonInner>
        );
    }

}

ButtonModal.propTypes = {
    backgroundColor: PropTypes.string,
    text: PropTypes.string,
}

export default ButtonModal;