import React, {Component} from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

class HeaderH2 extends Component {

    constructor() {
        super();
        this.Title = styled.h2`
            position: relative;
            margin: 0px;
            padding: 25px 30px;
            font-size: 20px;
            color: #fff;
            background-color: #d64531;
        `;
    }
    
    render() {
        return (
            <this.Title>
                {this.props.title}
            </this.Title>
        )
    }
}

HeaderH2.propTypes = {
    title: PropTypes.string,
}

export default HeaderH2;