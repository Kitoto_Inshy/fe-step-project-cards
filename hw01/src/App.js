import React, { Component } from 'react';
import Modal from './Components/Modal/Modal';
import HelpDiv from './Components/Containers/HelpDiv/HelpDiv';
import Button from './Components/Button/Button';

class App extends Component {

    constructor() {
        super();
        this.state = {
            isDelModal: false,
            isAddModal: false,
        };
        this.buttonsForDeleteModal = [
            {
                text: 'OK',
                backgroundColor: '#b53726',
            },
            {
                text: 'Cancel',
                backgroundColor: '#b53726',
            }
        ];
        this.buttonsForAddModal = [
            {
                text: 'Accept',
                backgroundColor: '#5fb526',
            },
            {
                text: 'Dismiss',
                backgroundColor: '#5fb526',
            }
        ];
    }

    showDelModal = () => {
        this.setState({ isDelModal: true });
    }

    showAddModal = () => {
        this.setState({ isAddModal: true });
    }

    hideDelModal = () => {
        this.setState({ isDelModal: false });
    }

    hideAddModal = () => {
        this.setState({ isAddModal: false });
    }

    render() {
        return (
            <div>
                <Modal header='Do you want to delete this file?'
                    text1='Once you delete this file, it wont to undo this action.'
                    text2='Are you sure to delete this?'
                    buttons={this.buttonsForDeleteModal}
                    show={this.state.isDelModal}
                    hide={() => this.hideDelModal()}
                />

                <Modal header='Do you want to add this file?'
                    text1='Once you add this file, it wont to undo this action.'
                    text2='Are you sure to add this?'
                    buttons={this.buttonsForAddModal}
                    show={this.state.isAddModal}
                    hide={() => this.hideAddModal()}
                />

                <HelpDiv>
                    <Button text='Open first modal' backgroundColor='#e94b35'
                        todo={() => this.showDelModal()} />
                    <Button text='Open second modal' backgroundColor='#61cc20'
                        todo={() => this.showAddModal()} />
                </HelpDiv>
            </div >
        )
    }
}

export default App;
