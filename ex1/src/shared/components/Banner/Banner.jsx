import React from "react";

import "./Banner.sass";

import { Button } from "../Button";

export const Banner = () => {

    return (
        <div className="banner">
            <small className="banner-info">
                Recive information on the lastest hit movies straight to your inbox
            </small>
            <Button text="Subscribe me" btnClass="btn-banner btn-main" />
        </div>
    )
}