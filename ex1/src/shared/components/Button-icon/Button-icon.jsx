import React from "react";

import "./Button-icon.sass";

export const ButtonIcon = (props) => {

    return (
        <button className="btn btn-icon">
            {props.type}
        </button>
    )
}