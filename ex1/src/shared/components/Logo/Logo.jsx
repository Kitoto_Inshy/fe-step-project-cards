import React from "react";

import "./Logo.sass";

export const Logo = (props) => {

    return (
        <h1 className="logo" style={{color : [props.color]}}>
            <span className="logo-part-one">Movie</span>
            <span className="logo-part-two">Rise</span>
        </h1>
    )
}