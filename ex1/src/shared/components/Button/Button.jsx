import React from "react";

import "./Button.sass";

export const Button = (props) => {

    const btnClass = props.btnClass ? `btn self-btn ${props.btnClass}` : "btn self-btn";

    return (
        <button className={btnClass}>
            {props.text}
        </button>
    )
}