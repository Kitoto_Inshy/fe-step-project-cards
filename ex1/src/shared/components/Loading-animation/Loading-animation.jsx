import React from "react";

import "./Loading-animation.sass";

export const LoadingAnimation = () => {

    return (
        <div className="loading">
            <div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div>
            <small className="loading-info">
                Loading
            </small>
        </div>
    )
}