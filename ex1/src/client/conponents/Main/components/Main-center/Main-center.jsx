import React from "react";

import "./Main-center.sass";

import { MainItem } from "../Main-item";
import { Banner } from "../../../../../shared/components/Banner";
import { LoadingAnimation } from "../../../../../shared/components/Loading-animation";

export const MainCenter = () => {

    const films = [
        {
            poster: "https://res.cloudinary.com/kitotoinshy/image/upload/v1596564946/react/hw01/01_fviefe.png",
            title: "Fantastic Beasts",
            mark: 4.7,
            genres: "Adventure, Family, Fantasy"
        },
        {
            poster: "https://res.cloudinary.com/kitotoinshy/image/upload/v1596564949/react/hw01/02_qjbbef.png",
            title: "Assassin`s Creed",
            mark: 4.8,
            genres: "Adventure, Family, Fantasy"
        },
        {
            poster: "https://res.cloudinary.com/kitotoinshy/image/upload/v1596564952/react/hw01/03_qhhadj.png",
            title: "Now You See Me 2.0",
            mark: 4.6,
            genres: "Adventure, Family, Fantasy"
        },
        {
            poster: "https://res.cloudinary.com/kitotoinshy/image/upload/v1596564956/react/hw01/04_vfofcy.png",
            title: "Tarzan",
            mark: 4.7,
            genres: "Adventure, Family, Fantasy"
        },
        {
            poster: "https://res.cloudinary.com/kitotoinshy/image/upload/v1596564959/react/hw01/05_mnsghy.png",
            title: "Doctor STR",
            mark: 4.4,
            genres: "Adventure, Family, Fantasy"
        },
        {
            poster: "https://res.cloudinary.com/kitotoinshy/image/upload/v1596564962/react/hw01/06_uhifle.png",
            title: "Capitan America",
            mark: 4.9,
            genres: "Adventure, Family, Fantasy"
        },
        {
            poster: "https://res.cloudinary.com/kitotoinshy/image/upload/v1596564964/react/hw01/07_fmd9vp.png",
            title: "Finding Dory",
            mark: 4.7,
            genres: "Adventure, Family, Fantasy"
        },
        {
            poster: "https://res.cloudinary.com/kitotoinshy/image/upload/v1596564968/react/hw01/08_utvxdm.png",
            title: "Moana",
            mark: 4.5,
            genres: "Adventure, Family, Fantasy"
        },
        {
            poster: "https://res.cloudinary.com/kitotoinshy/image/upload/v1596564956/react/hw01/04_vfofcy.png",
            title: "Tarzan",
            mark: 4.7,
            genres: "Adventure, Family, Fantasy"
        },
        {
            poster: "https://res.cloudinary.com/kitotoinshy/image/upload/v1596564959/react/hw01/05_mnsghy.png",
            title: "Doctor STR",
            mark: 4.4,
            genres: "Adventure, Family, Fantasy"
        },
        {
            poster: "https://res.cloudinary.com/kitotoinshy/image/upload/v1596564962/react/hw01/06_uhifle.png",
            title: "Capitan America",
            mark: 4.9,
            genres: "Adventure, Family, Fantasy"
        },
        {
            poster: "https://res.cloudinary.com/kitotoinshy/image/upload/v1596564964/react/hw01/07_fmd9vp.png",
            title: "Finding Dory",
            mark: 4.7,
            genres: "Adventure, Family, Fantasy"
        },
    ]

    const banner = <Banner />

    const filmsItems = films.map (elem => {

        return (
            <MainItem {...elem}/>
        )
    });

    filmsItems.splice(8, 0, banner);

    return (
        <div className="main-center">
            {filmsItems}
            <LoadingAnimation />
        </div>
    )
}