import React from "react";

import "./Main-navbar.sass";

import { MainNavbarItem } from "../Main-navbar-item";
import { ButtonIcon } from "../../../../../shared/components/Button-icon";

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

export const MainNavbar = () => {

    const mainNavbarItemsArr = ["Tranding", "Top Rated", "New Arrivals", "Trailers", "Coming Soon", "Genre +"];
    const mainNavbarItems = mainNavbarItemsArr.map(elem => {

        return (
            <MainNavbarItem text={elem} />
        )
    })
    const btnGrid4 = <FontAwesomeIcon icon="th-large" />;
    const btnGrid2 = <FontAwesomeIcon icon="bars" />

    return (

        <div className="main-navbar">
            <div className="main-navbar-links">
                {mainNavbarItems}
            </div>
            <div className="main-navbar-buttons">
                <ButtonIcon type={btnGrid4} />
                <ButtonIcon type={btnGrid2} />
            </div>
        </div>
    )
}