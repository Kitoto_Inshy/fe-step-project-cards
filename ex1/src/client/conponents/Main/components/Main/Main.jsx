import React, {Component} from "react";

import "./Main.sass";

import { MainNavbar } from "../Main-navbar";
import { MainCenter} from "../Main-center";

class Main extends Component {

    render() {

        return (
            <main className="main">
                <MainNavbar />
                <MainCenter />
            </main>
        )
    }
}

export default Main;