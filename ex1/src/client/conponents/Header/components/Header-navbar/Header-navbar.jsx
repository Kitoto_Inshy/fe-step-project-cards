import React from "react";

import "./Header-navbar.sass";

import { Logo } from "../../../../../shared/components/Logo";
import { Button } from "../../../../../shared/components/Button";
import { ButtonIcon } from "../../../../../shared/components/Button-icon";

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

export const HeaderNavbar = () => {

    const btnType = <FontAwesomeIcon icon="search" />;

    return (
        <div className="header-navbar">
            <Logo />
            <div className="header-navbar-sign">
                <ButtonIcon type={btnType} />
                <Button text="Sign In"/>
                <Button text="Sign Up" btnClass="btn-main"/>
            </div>
        </div>
    )
}