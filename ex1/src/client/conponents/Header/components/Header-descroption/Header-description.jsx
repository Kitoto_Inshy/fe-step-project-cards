import React from "react";

import "./Header-description.sass";

import { Button } from "../../../../../shared/components/Button";
import { ButtonIcon } from "../../../../../shared/components/Button-icon";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

export const HeaderDescription = () => {

    const infoItemsArr = ["Adventure", "Drama", "Family", "Fantasy", "I", "1h 46m"];
    const infoItems = infoItemsArr.map((elem) => {
        return (
            <small className="header-description-ti-info-item">
                {elem}
            </small>
        )
    })

    const btnIcon = <FontAwesomeIcon icon="ellipsis-v" />

    return (
        <div className="header-description">
            <div className="header-description-ti">
                <h3 className="header-description-ti-title">
                    The Jungle Book
                </h3>
                <div className="header-description-ti-info">
                    {infoItems}
                </div>
                <div className="header-description-ti-mark">
                    <FontAwesomeIcon icon="star" className="icon" />
                    <FontAwesomeIcon icon="star" className="icon" />
                    <FontAwesomeIcon icon="star" className="icon" />
                    <FontAwesomeIcon icon="star" className="icon" />
                    <FontAwesomeIcon icon="star" className="icon" />
                    <small className="header-description-ti-mark-integer">
                        4.8
                    </small>
                </div>
            </div>
            <div className="header-description-options">
                <Button text="Watch Now" btnClass="btn-main"/>
                <Button text="View Info" btnClass="btn-outline-dark"/>
                <Button text="+ Favorites" />
                <ButtonIcon type={btnIcon} />
            </div>
        </div>
    )
}