import React, { Component } from "react";

import "./Header.sass";

import { HeaderNavbar } from "../Header-navbar";
import { HeaderSliaderbar } from "../Header-sliderbar";
import { HeaderDescription } from "../Header-descroption";

class Header extends Component {
    
    render() {
        return (
            <header className="header">
                <HeaderNavbar />
                <HeaderSliaderbar />
                <HeaderDescription />
            </header>
        )
    }
}

export default Header;