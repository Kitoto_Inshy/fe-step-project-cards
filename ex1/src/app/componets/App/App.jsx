import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../../../shared/styles/sass/style.sass';

import { Header } from "../../../client/conponents/Header/components/Header";
import { Main } from "../../../client/conponents/Main/components/Main";
import { Footer } from "../../../client/conponents/Footer";

import { library } from '@fortawesome/fontawesome-svg-core';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { faStar } from '@fortawesome/free-solid-svg-icons';
import { faEllipsisV } from '@fortawesome/free-solid-svg-icons';
import { faThLarge } from '@fortawesome/free-solid-svg-icons';
import { faBars } from '@fortawesome/free-solid-svg-icons';
import { fab } from '@fortawesome/free-brands-svg-icons'

library.add(fab, faSearch, faStar, faEllipsisV, faThLarge, faBars);


export const App = () => {
    return (
        <div className="container">
            <Header />
            <Main />
            <Footer />
        </div>
    );
};
