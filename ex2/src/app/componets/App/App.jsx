import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../../../shared/styles/css/style.css';
import { v4 as uuidv4 } from 'uuid';

import { Header } from '../../../client/components/Header';
import { AddBookForm } from '../../../shared/components/AddBookForm';
import { AddBookFormItem } from '../../../shared/components/AddBookFormItem';
import { AddBookFormBtn } from '../../../shared/components/AddBookFormBtn';
import { AmountOfBooks } from '../../../client/components/AmountOfBooks';
import { BooksList } from '../../../client/components/BooksList';
import { BookListItem } from '../../../client/components/BookListItem';
import { ChangeBookForm } from '../../../shared/components/ChangeBookForm';

class App extends Component {
  state = {
    bookName: '',
    bookAuthor: '',
    bookIsbn: '',
    books: [],
  };

  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  addBook = event => {
    this.state.books.push({
      id: uuidv4(),
      bookName: [this.state.bookName],
      bookAuthor: [this.state.bookAuthor],
      bookIsbn: [this.state.bookIsbn],
      inChange: false,
    });
    this.setState({
      books: this.state.books,
    });
    event.preventDefault();
    event.target.reset();
  };

  handleRemoveItem = idItem => {
    const updatedBooks = [...this.state.books];
    const foundItemIndex = this.state.books.findIndex(
      ({ id }) => id === idItem,
    );
    updatedBooks.splice(foundItemIndex, 1);
    this.setState({ books: updatedBooks });
  };

  handleChangeItem = idItem => {
    const changedBooks = [...this.state.books];
    const foundItemIndex = this.state.books.findIndex(
      ({ id }) => id === idItem,
    );
    const foundItem = this.state.books.find(({ id }) => id === idItem);
    foundItem.inChange = !foundItem.inChange;
    const newBooks = [
      ...changedBooks.splice(foundItemIndex, 1),
      foundItem,
      ...changedBooks.splice(foundItemIndex + 1),
    ];
    this.setState({ books: newBooks });
  };

  render() {
    const bookItems = this.state.books.map(elem => {
      return (
        <BookListItem
          handleRemoveItem={() => this.handleRemoveItem(elem.id)}
          handleChangeItem={() => this.handleChangeItem(elem.id)}
          {...elem}
        />
      );
    });

    return (
      <div className="container">
        <Header />
        <div className="row">
          <div className="col-lg-4">
            <AddBookForm addBook={this.addBook}>
              <AddBookFormItem
                value={this.state.bookName}
                handleChange={this.handleChange}
                title="Title"
                type="text"
                name="bookName"
              />
              <AddBookFormItem
                value={this.state.bookAuthor}
                handleChange={this.handleChange}
                title="Author"
                type="text"
                name="bookAuthor"
              />
              <AddBookFormItem
                value={this.state.bookIsbn}
                handleChange={this.handleChange}
                title="ISBN#"
                type="text"
                name="bookIsbn"
              />
              <AddBookFormBtn type="submit" value="Add book" />
            </AddBookForm>
          </div>
        </div>
        <AmountOfBooks amount={this.state.books.length} />
        <BooksList>
          {bookItems}
          <ChangeBookForm />
        </BooksList>
      </div>
    );
  }
}

export default App;
