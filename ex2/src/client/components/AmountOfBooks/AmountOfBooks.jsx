import React from 'react';

export const AmountOfBooks = props => {
  let { amount } = props;
  return (
    <h3 id="book-count" className="book-count mt-5">
      Amount of books: {amount}
    </h3>
  );
};
