import React from 'react';

import { Logo } from '../../../shared/components/Logo';

export const Header = () => {
  return (
    <div>
      <Logo />
      <p className="text-center">
        Add your book information to store it in database.
      </p>
    </div>
  );
};
