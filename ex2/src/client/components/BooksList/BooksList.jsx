import React from 'react';

export const BooksList = props => {
  let { children } = props;
  return (
    <table className="table table-striped mt-2">
      <thead>
        <tr>
          <th>Title</th>
          <th>Author</th>
          <th>ISBN#</th>
          <th>Title</th>
        </tr>
      </thead>
      <tbody id="book-list">{children}</tbody>
    </table>
  );
};
