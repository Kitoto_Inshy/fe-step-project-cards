import React from 'react';
import { v4 as uuidv4 } from 'uuid';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit } from '@fortawesome/free-solid-svg-icons';

export const BookListItem = props => {
  let {
    bookName,
    bookAuthor,
    bookIsbn,
    handleChangeItem,
    handleRemoveItem,
  } = props;
  return (
    <tr key={uuidv4()}>
      <td>{bookName}</td>
      <td>{bookAuthor}</td>
      <td>{bookIsbn}</td>
      <td>
        <a
          href="#"
          onClick={() => handleChangeItem()}
          className="btn btn-info btn-sm">
          <FontAwesomeIcon icon={faEdit} />
        </a>
      </td>
      <td>
        <a
          href="#"
          onClick={() => handleRemoveItem()}
          className="btn btn-danger btn-sm btn-delete">
          X
        </a>
      </td>
    </tr>
  );
};
