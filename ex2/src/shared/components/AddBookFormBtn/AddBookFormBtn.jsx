import React from 'react';

export const AddBookFormBtn = props => {
  let { type, value } = props;
  return <input type={type} value={value} className="btn btn-primary" />;
};
