import React from 'react';

export const ChangeBookForm = () => {
  return (
    <tr>
      <td>
        <div className="form-group">
          <input
            type="text"
            name="bookName"
            className="form-control"
            value="Название книги"></input>
        </div>
      </td>
      <td>
        <div className="form-group">
          <input
            type="text"
            name="bookAuthor"
            className="form-control"
            value="ISBN книги"></input>
        </div>
      </td>
      <td>
        <div className="form-group">
          <input
            type="text"
            name="bookIsbn"
            className="form-control"
            value="ISBN книги"></input>
        </div>
      </td>
      <td>
        <input type="submit" value="Update" className="btn btn-primary"></input>
      </td>
    </tr>
  );
};
