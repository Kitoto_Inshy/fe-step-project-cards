import React from 'react';

export const AddBookForm = props => {
  let { addBook, children } = props;
  return (
    <form id="add-book-form" onSubmit={addBook}>
      {children}
    </form>
  );
};
