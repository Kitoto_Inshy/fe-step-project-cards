import React from 'react';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBookOpen } from '@fortawesome/free-solid-svg-icons';

export const Logo = () => {
  return (
    <h1 className="display-4 text-center">
      <FontAwesomeIcon icon={faBookOpen} />
      <span className="text-secondary">Book</span> List
    </h1>
  );
};
