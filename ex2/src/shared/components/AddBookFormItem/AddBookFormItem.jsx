import React from 'react';

export const AddBookFormItem = props => {
  let { title, type, name, handleChange } = props;
  return (
    <div className="form-group">
      <label htmlFor="title">{title}</label>
      <input
        type={type}
        name={name}
        onChange={handleChange}
        className="form-control"
      />
    </div>
  );
};
